var countries = [
  {
    name: 'india',
    flagImage:
      'https://www.searchpng.com/wp-content/uploads/2019/05/Flag-of-India-Round-India-Cricket-Flag-715x715.jpg',
    cities: [
      {
        name: 'kochi',
        whether: '30'
      },
      {
        name: 'chennai',
        whether: '38'
      },
      {
        name: 'bangalore',
        whether: '28'
      }
    ]
  },
  {
    name: 'Afghanistan',
    flagImage:
      'https://i2.wp.com/www.mtctutorials.com/wp-content/uploads/2019/03/Afghanistan-Flag-png-by-mtc-tutorials.png?ssl=1',
    cities: [
      {
        name: 'herat',
        whether: '40'
      },
      {
        name: 'kabul',
        whether: '42'
      },
      {
        name: 'kandhar',
        whether: '30'
      }
    ]
  },
  {
    name: 'Sri Lanka',
    flagImage:
      'https://cleanclothes.org/image-repository/ua-2013-img-640px-flag-map-of-sri-lanka-svg.png/@@images/image.png',
    cities: [
      {
        name: 'jaffna',
        whether: '30'
      },
      {
        name: 'colombo',
        whether: '38'
      },
      {
        name: 'kandy',
        whether: '28'
      }
    ]
  }
];
